# -*- coding: utf-8 -*-
"""
Spyder Editor

Author: Michael Harris
Purpose: A script based on lesson 2 of Spatial Data Workshop
"""

import rasterio as rio
from rasterio.plot import show
from rasterio.plot import show_hist
import matplotlib.pyplot as plt
import numpy as np
import os
from shapely.geometry import Polygon, mapping
from rasterio.mask import mask
import earthpy as et
import earthpy.spatial as es

# plotting in line
plt.ion()

# setting work directory
os.chdir('G:/python/repos/earth-analytics')

sjer_dtm_path = "data/spatial-vector-lidar/california/neon-sjer-site/2013/lidar/SJER_lidarDSM.tif"

# open raster data with context manager (the best way)
with rio.open(sjer_dtm_path) as src:
    # convert/read data into a numpy array. Masked to prevent NAN
    lidar_dem_im = src.read(1, masked=True)
    # Making extent
    sjer_ext = rio.plot.plotting_extent(src)

# Plotting DEM
fig, ax = plt.subplots(figsize=(10, 10))
lidar_plot = ax.imshow(lidar_dem_im, cmap='Greys', extent=sjer_ext)
ax.set_title("Lidar Digital Elevation Model \n something", fontsize=20)
es.colorbar(lidar_plot)
ax.set_axis_off()

# create histogram of lidar Data
fig2, ax2 = plt.subplots(figsize = (8,6))
show_hist(lidar_dem_im, facecolor='purple', ax=ax2, bins=100, title='DEM \n Distribution of Elevation Values')
