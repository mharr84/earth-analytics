# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 15:55:52 2019

@author: demib84
"""

import rasterio as rio
from rasterio.plot import show
from rasterio.plot import show_hist
import matplotlib.pyplot as plt
import numpy as np
import os
from shapely.geometry import Polygon, mapping
from rasterio.mask import mask
import earthpy as et
import earthpy.spatial as es
from matplotlib.colors import ListedColormap
import matplotlib.colors as colors

# plotting inline in the notebook
plt.ion()

# Setting working directory
os.chdir('G:/python/repos/earth-analytics/')

# Opening created geoTiff from Lesson 2
with rio.open('data/spatial-vector-lidar/spatial/outputs' +
                   '/lidar_chm.tiff') as src:
    chm = src.read(1, masked=True)
    
print(chm.shape)