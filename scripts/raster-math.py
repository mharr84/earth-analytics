# -*- coding: utf-8 -*-
"""
Author: Michael Harris
Purpose: Based on lesson 3 of the Spatial Data Workshop.
"""

import rasterio as rio
from rasterio.plot import show
from rasterio.plot import show_hist
import matplotlib.pyplot as plt
import numpy as np
import os
from shapely.geometry import Polygon, mapping
from rasterio.mask import mask
import earthpy as et
import earthpy.spatial as es
from matplotlib.colors import ListedColormap
import matplotlib.colors as colors

# plotting inline in the notebook
plt.ion()

# Setting working directory
os.chdir('G:/python/repos/earth-analytics/')

sjer_dtm_path = ("data/spatial-vector-lidar/california/neon-soap-site" +
                 "/2013/lidar/SOAP_lidarDTM.tif")

with rio.open(sjer_dtm_path) as src:
    lidar_dem_im = src.read(1, masked=True)
    sjer_ext = rio.plot.plotting_extent(src)

# open the digital terrain model
sjer_dsm_path = ("data/spatial-vector-lidar/california/neon-soap-site" +
                 "/2013/lidar/SOAP_lidarDSM.tif")

with rio.open(sjer_dsm_path) as src:
    lidar_dsm_im = src.read(1, masked=True)
    dsm_meta = src.profile

# Subtracting DEM from DSM to determine CHM
lidar_chm = lidar_dsm_im - lidar_dem_im

# Displaying CHM
fig, ax = plt.subplots(figsize=(10,10))
chm_plot = ax.imshow(lidar_chm, cmap='viridis')
ax.set_axis_off()
es.colorbar(chm_plot)
ax.set_title(('Lidar Canopy Height Model\n Tree Height For Your' + 
              'Field Site From Remote Sensing Data'), fontsize=16)

#View histogram of the data
fig, ax = plt.subplots(figsize=(10,10))
show_hist(lidar_chm,
          facecolor='purple',
          bins=100,
          title='Distribution of Canopy High Model Pixels')

# Exploring CHM
print('CHM minimum value: ', lidar_chm.min())
print('CHM maximum value: ', lidar_chm.max())

# Defining colors for a range of heights
cmap = ListedColormap(['white', 'tan', 'springgreen', 'darkgreen'])

# Defining a normalization from values ->
norm = colors.BoundaryNorm([0,2,10,20,30], 5)

# Displaying more defined CHM
fig, ax = plt.subplots(figsize=(12, 8))
chm_plot = ax.imshow(lidar_chm, cmap=cmap, norm=norm)
ax.set_title('Lidar Canopy Height Model (CHM', fontsize=16)
es.colorbar(chm_plot)
ax.set_axis_off()

# Creating hillshade using the hillshade function from earthpy
chm_hill = es.hillshade(lidar_chm, 315, 45)

# Displaying defined CHM with hillshade
fig, ax = plt.subplots(figsize=(20, 20))
ax.imshow(chm_hill, cmap='Greys')
chm_plot = ax.imshow(lidar_chm,
                     cmap=cmap,
                     norm=norm, alpha=0.65)

es.colorbar(chm_plot)
ax.set_title("Lidar Canopy Height Model (CHM)", fontsize=16)

# Exporting raster using rasterio
if os.path.exists('data/spatial-vector-lidar/spatial/outputs'):
    print('The directory exists!')
else:
    os.makedirs('data/spatial-vector-lidar/spatial/outputs')

with rio.open('data/spatial-vector-lidar/spatial/outputs/lidar_chm.tiff',
              'w', **dsm_meta) as ff:
    ff.rasterio.write(lidar_chm,1)
